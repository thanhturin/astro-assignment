# RELEASE NOTE #

### Version 1.0.0 ###

* Displaying a list of Astro channels (showing at a minimum channel name and number)	* Ability to sort by channel number	* Ability to sort by channel name* Allowing a user to mark a favourite channel as on-device persistent.	* User launches app and tags a favourite channel	* User closes app and favourite channel appears in main screen identified as a favourite

* Displaying a **TVguide **	* Displaying the current show for all channels currently airing current time	* Can sort by channel number or channel name 	* Pagination is allowed	* `Lazy loading`