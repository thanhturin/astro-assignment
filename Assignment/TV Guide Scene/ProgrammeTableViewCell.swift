//
//  ProgrammeTableViewCell.swift
//  Assignment
//
//  Created by Thanh KFit on 3/8/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Kingfisher

final class ProgrammeTableViewCell: UITableViewCell {

    @IBOutlet fileprivate weak var programmeTitleLabel: UILabel!

    @IBOutlet fileprivate weak var channelTitleLabel: UILabel!

    @IBOutlet fileprivate weak var channelNumberLabel: UILabel!

    @IBOutlet weak var favoriteButton: DOFavoriteButton!

    @IBOutlet weak var timeLabel: UILabel!

    var viewModel: ProgrammeTableViewCellViewModel! {
        didSet {
            bind()
        }
    }

    @IBAction func didTapFavoriteButton(_ sender: DOFavoriteButton) {
        if sender.isSelected {
            sender.deselect()
        } else {
            sender.select()
        }
        viewModel.updateFavoritedChannel.onNext()
    }
}

extension ProgrammeTableViewCell: ViewModelBindable {
    func bind() {
        _ = viewModel.programmeTitle.drive(programmeTitleLabel.rx.text)
        _ = viewModel.channelId.drive(channelNumberLabel.rx.text)
        _ = viewModel.channelTitle.drive(channelTitleLabel.rx.text)
        _ = viewModel.timeString.drive(timeLabel.rx.text)

        _ = viewModel.isFavoritedChannel
            .distinctUntilChanged()
            .driveNext { [unowned self] (isFavoritedChannel: Bool) in
                self.favoriteButton.isSelected = isFavoritedChannel
            }
    }
}
