//
//  TVGuideViewControllerViewModel.swift
//  Assignment
//
//  Created by Thanh KFit on 3/7/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

final class TVGuideViewControllerViewModel: ViewModel {

    // MARK: - Dependency
    fileprivate let eventsAPI: EventsAPI
    fileprivate let channelListAPI: ChannelListAPI

    let channelList: Variable<[Channel]> = Variable([Channel]())
    let events: Variable<[Event]> = Variable([Event]())

    let sortByChannelNumber = PublishSubject<Void>()
    let sortByChannelName = PublishSubject<Void>()

    let currentPage = Variable(0)

    let loadNextPage = PublishSubject<Void>()
    var isLoading = false
    let notLoading = Variable(false)
    let limitPerPage = 10

    init(
        eventsAPI: EventsAPI = EventsAPIDefault()
        , channelListAPI: ChannelListAPI = ChannelListAPIDefault()
    ) {
        self.channelListAPI = channelListAPI
        self.eventsAPI = eventsAPI

        super.init()

        getChannelList()

        Observable.combineLatest(
            currentPage.asObservable(),
            channelList.asObservable()) { [unowned self](currentPage: Int, channelList: [Channel]) -> [Int] in

            if channelList.isEmpty || currentPage * self.limitPerPage > channelList.count {
                self.notLoading.value = true
                return []
            }

            self.notLoading.value = false
            let currentIndex = currentPage * self.limitPerPage
            let destinationIndex = min((currentPage + 1) * self.limitPerPage - 1, channelList.count - 1)
            let ids = channelList.map { return $0.channelId }[currentIndex ..< destinationIndex]
            return Array(ids)
        }.subscribeNext { [unowned self] (channelIds: [Int]) in
            self.getEvents(channelIds: channelIds)
        }.addDisposableTo(disposeBag)

        loadNextPage
            .subscribeNext { [unowned self] () in
                if self.channelList.value.isEmpty {
                    return
                }

                if self.currentPage.value * self.limitPerPage > self.channelList.value.count {
                    return
                }

                if self.isLoading {
                    return
                }

                self.currentPage.value = self.currentPage.value + 1
            }.addDisposableTo(disposeBag)

        sortByChannelNumber
            .subscribeNext {
                [unowned self] in
                let channelListSortedByNumber = self.events.value.sorted(by: { (left, right) -> Bool in
                    left.channelStbNumber < right.channelStbNumber
                })
                self.events.value = channelListSortedByNumber
            }.addDisposableTo(disposeBag)

        sortByChannelName
            .subscribeNext {
                [unowned self] in
                let channelListSortedByName = self.events.value.sorted(by: { (left, right) -> Bool in
                    left.channelTitle < right.channelTitle
                })
                self.events.value = channelListSortedByName
            }.addDisposableTo(disposeBag)
    }

    func refresh() {
        events.value = [Event]()
        currentPage.value = 0
    }

    func getEvents(channelIds: [Int]) {
        if channelIds.isEmpty {
            return
        }

        if isLoading {
            return
        }

        let currentTime = Date()
        let time4HoursAgo = currentTime.addingTimeInterval(-4 * 60 * 60)

        let requestPayload = EventsAPIRequestPayload(channelId: channelIds, periodStart: time4HoursAgo.UTCDateTimeString, periodEnd: currentTime.UTCDateTimeString)
        let request: Observable<EventsAPIResponsePayload>

        //        if currentPage.value == 0 {
        //            request = eventsAPI
        //                .request(withRequestPayload: requestPayload)
        //                .trackActivity(app.activityIndicator)
        //        } else {
        //            request = eventsAPI
        //                .request(withRequestPayload: requestPayload)
        //        }
        request = eventsAPI
            .request(withRequestPayload: requestPayload)

        request
            .doOnError({ [unowned self] (error: Error) in
                self.controllerNavigator.navigate.onNext({ (viewController: UIViewController) in
                    viewController.present(UIAlertController.alertController(forError: error, actions: nil), animated: true, completion: nil)
                })
            })
            .doOnCompleted { [unowned self] in
                self.isLoading = false
            }
            .map({ (response: EventsAPIResponsePayload) -> [Event] in
                let currentEvents = response.events.filter({ (event: Event) -> Bool in
                    let now = Date()
                    let eventStartTime = event.displayDateTime
                    let eventEndTime = eventStartTime.addingTimeInterval(event.displayDuration)
                    return now.laterThanDate(eventStartTime) && now.earlierThanDate(eventEndTime)
                })
                return currentEvents
            })
            .subscribeNext { [unowned self] (events: [Event]) in
                self.events.value = self.events.value + events
            }.addDisposableTo(disposeBag)
    }

    func getChannelList() {
        _ = channelListAPI
            .request(withRequestPayload: ChannelListAPIRequestPayload())
            //            .trackActivity(app.activityIndicator)
            .subscribe(
                onNext: { [unowned self] (response: ChannelListAPIResponsePayload) in
                    self.channelList.value = response.channels
                },
                onError: { [unowned self] (error: Error) in
                    self.controllerNavigator.navigate.onNext({ (viewController: UIViewController) in
                        viewController.present(UIAlertController.alertController(forError: error, actions: nil), animated: true, completion: nil)
                    })
            })
            .addDisposableTo(disposeBag)
    }
}
