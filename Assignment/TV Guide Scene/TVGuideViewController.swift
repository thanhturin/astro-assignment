//
//  TVGuideViewController.swift
//  Assignment
//
//  Created by Thanh KFit on 3/7/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

final class TVGuideViewController: ViewController {

    // MARK: - IBOutlet
    @IBOutlet weak var tableView: UITableView!
    let refreshController = UIRefreshControl()
    var spinner: UIActivityIndicatorView!

    // MARK: - ViewModel
    var viewModel: TVGuideViewControllerViewModel!

    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = TVGuideViewControllerViewModel()
        configureTableView()
        configureNavigationBar()
        bind()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel.setAsActive()
    }

    func configureTableView() {
        tableView.tableFooterView = UIView()
        tableView.register(UINib(nibName: "ProgrammeTableViewCell", bundle: nil), forCellReuseIdentifier: "ProgrammeTableViewCell")
        setupRefreshControl()
        setupFooter()
    }

    func setupRefreshControl() {
        refreshController.rx.controlEvent(UIControlEvents.valueChanged)
            .subscribeNext { [weak self] in
                self?.viewModel.refresh()
            }.addDisposableTo(disposeBag)

        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshController
        } else {
            tableView.addSubview(refreshController)
        }
    }

    func setupFooter() {
        spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        spinner.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50)
        spinner.startAnimating()
        tableView.tableFooterView = spinner
    }

    func configureNavigationBar() {

        navigationItem.title = "TV Guide"

        // Add Sort button
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Sort", style: UIBarButtonItemStyle.plain, target: self, action: #selector(didTapSortButton))
        navigationItem.rightBarButtonItem!.setTitleTextAttributes([NSFontAttributeName: UIFont.systemFont(ofSize: 15)], for: UIControlState())
        navigationItem.rightBarButtonItem?.tintColor = UIColor.red
    }

    func didTapSortButton() {
        let actionSheet = UIAlertController(title: "Sort By", message: "", preferredStyle: UIAlertControllerStyle.actionSheet)

        actionSheet.addAction(UIAlertAction(title: "Channel Name", style: UIAlertActionStyle.default, handler: { [weak self] _ in
            self?.viewModel.sortByChannelName.onNext()
        }))

        actionSheet.addAction(UIAlertAction(title: "Channel Number", style: UIAlertActionStyle.default, handler: { [weak self] _ in
            self?.viewModel.sortByChannelNumber.onNext()
        }))

        present(actionSheet, animated: true, completion: nil)
    }
}

extension TVGuideViewController: UITableViewDataSource {
    func numberOfSections(in _: UITableView) -> Int {
        return 1
    }

    func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
        return viewModel.events.value.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let event = viewModel.events.value[indexPath.row]
        let cellViewModel = ProgrammeTableViewCellViewModel(event: event)
        let cell: ProgrammeTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ProgrammeTableViewCell", for: indexPath) as! ProgrammeTableViewCell
        cell.viewModel = cellViewModel
        return cell
    }
}

extension TVGuideViewController: UITableViewDelegate {
    func tableView(_: UITableView, heightForRowAt _: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }

    func tableView(_: UITableView, estimatedHeightForRowAt _: IndexPath) -> CGFloat {
        return 100
    }

    func tableView(_ tableView: UITableView, willDisplay _: UITableViewCell, forRowAt indexPath: IndexPath) {
        let numberOfRow = tableView.numberOfRows(inSection: 0)

        if numberOfRow == 0 {
            return
        }

        let currentRow = indexPath.row
        if currentRow > numberOfRow - 10 {
            viewModel.loadNextPage.onNext()
        }
    }
}

// MARK: - ViewModelBinldable
extension TVGuideViewController: ViewModelBindable {
    func bind() {
        viewModel
            .controllerNavigator
            .navigate
            .filter { [weak self] _ -> Bool in
                guard let strongSelf = self else { return false }
                return strongSelf.viewModel.isActive
            }
            .subscribeNext { [weak self] navigationClosure in
                if let strongSelf = self {
                    navigationClosure(strongSelf)
                }
            }
            .addDisposableTo(disposeBag)

        viewModel
            .events
            .asDriver()
            .driveNext { [weak self] _ in
                self?.tableView.reloadData()
            }.addDisposableTo(disposeBag)

        //        Driver.combineLatest(
        //            viewModel.notLoading.asDriver().debug("Not Loading"),
        //            viewModel.app.activityIndicator.asDriver().debug("Active")) { (notLoading, active: Bool) -> Bool in
        //                return notLoading == true && active == false
        //            }.drive(spinner.rx.isHidden)
        //            .addDisposableTo(disposeBag)

        viewModel
            .notLoading
            .asDriver()
            .drive(spinner.rx.isHidden)
            .addDisposableTo(disposeBag)

        viewModel
            .app
            .activityIndicator
            .driveNext({ [weak self] (active: Bool) in
                guard let strongSelf = self else { return }
                if active == false {
                    strongSelf.refreshController.endRefreshing()
                }
            })
            .addDisposableTo(disposeBag)
    }
}

// MARK: - Buildable
extension TVGuideViewController: Buildable {
    class func build(_ builder: TVGuideViewControllerViewModel) -> TVGuideViewController {
        let storyboard = UIStoryboard(name: "TVGuide", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "TVGuideViewController") as! TVGuideViewController
        vc.viewModel = builder
        return vc
    }
}
