//
//  Created by Thanh KFit on 3/8/17.
//    Copyright © 2017 Thanh KFit. All rights reserved.
//  See LICENSE.txt for license information
//

import Foundation
import RxSwift
import RxCocoa

class ProgrammeTableViewCellViewModel: ViewModel {

    // Input
    let event: Event

    // Output
    let channelTitle: Driver<String>
    let channelId: Driver<String>
    let programmeTitle: Driver<String>
    let timeString: Driver<String>
    let isFavoritedChannel: Driver<Bool>

    let updateFavoritedChannel = PublishSubject<Void>()

    init(
        event: Event
        , favoritedChannelService: FavoritedChannelService = favoritedChannelServiceDefault
    ) {

        self.event = event

        channelTitle = Driver.of(event.channelTitle)
        channelId = Driver.of("CH-\(event.channelStbNumber)")
        programmeTitle = Driver.of(event.programmeTitle)

        let startTimeString = event.displayDateTime.timeString
        let endTimeString = event.displayDateTime.addingTimeInterval(event.displayDuration).timeString
        timeString = Driver.of("\(startTimeString) - \(endTimeString):")

        isFavoritedChannel = favoritedChannelServiceDefault.favoritedChannels
            .asDriver()
            .map({ (channelsId: [Int]) -> Bool in
                let channelElements = channelsId.filter({ (_channelId: Int) -> Bool in
                    _channelId == event.channelId
                })
                return !channelElements.isEmpty
            })

        super.init()

        _ = updateFavoritedChannel.subscribeNext({ () in
            favoritedChannelService.updateFavoritedChannel(channelId: event.channelId)
        })
    }
}
