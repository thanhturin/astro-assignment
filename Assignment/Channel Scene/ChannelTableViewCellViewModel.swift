//
//  ChannelTableViewCellViewModel.swift
//  Assignment
//
//  Created by Thanh KFit on 3/5/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class ChannelTableViewCellViewModel: ViewModel {

    // Input
    let channel: Channel

    // Output
    let channelTitle: Driver<String>
    let channelId: Driver<String>

    let isFavoritedChannel: Driver<Bool>

    let updateFavoritedChannel = PublishSubject<Void>()

    init(
        channel: Channel
        , favoritedChannelService: FavoritedChannelService = favoritedChannelServiceDefault
    ) {

        self.channel = channel
        channelTitle = Driver.of(channel.channelTitle)
        channelId = Driver.of("CH-\(channel.channelStbNumber)")

        isFavoritedChannel = favoritedChannelServiceDefault.favoritedChannels
            .asDriver()
            .map({ (channelsId: [Int]) -> Bool in
                let channelElements = channelsId.filter({ (_channelId: Int) -> Bool in
                    _channelId == channel.channelId
                })
                return !channelElements.isEmpty
            })

        super.init()

        _ = updateFavoritedChannel.subscribeNext({ () in
            favoritedChannelService.updateFavoritedChannel(channelId: channel.channelId)
        })
    }
}
