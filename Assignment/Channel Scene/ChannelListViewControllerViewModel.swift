//
//  ChannelListViewControllerViewModel.swift
//  Assignment
//
//  Created by Thanh KFit on 3/5/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

final class ChannelListViewControllerViewModel: ViewModel {

    // MARK: - Dependency
    fileprivate let channelListAPI: ChannelListAPI
    let channelList: Variable<[Channel]> = Variable([Channel]())
    let sortByChannelNumber = PublishSubject<Void>()
    let sortByChannelName = PublishSubject<Void>()

    init(
        channelListAPI: ChannelListAPI = ChannelListAPIDefault()
    ) {
        self.channelListAPI = channelListAPI
        super.init()

        sortByChannelNumber
            .subscribeNext {
                [unowned self] in
                let channelListSortedByNumber = self.channelList.value.sorted(by: { (left, right) -> Bool in
                    left.channelStbNumber < right.channelStbNumber
                })
                self.channelList.value = channelListSortedByNumber
            }.addDisposableTo(disposeBag)

        sortByChannelName
            .subscribeNext {
                [unowned self] in
                let channelListSortedByName = self.channelList.value.sorted(by: { (left, right) -> Bool in
                    left.channelTitle < right.channelTitle
                })
                self.channelList.value = channelListSortedByName
            }.addDisposableTo(disposeBag)

        getChannelList()
        //        getChannelMetadata()
    }

    func getChannelMetadata() {
        let api = ChannelMetadataAPIDefault()
        let request = ChannelMetadataAPIRequestPayload(channelIds: ["1", "2"], languages: [], categories: [], platforms: [])

        _ = api.request(withRequestPayload: request)
            .subscribeNext({ _ in
                print("Next")
            })
    }

    func getChannelList() {
        _ = channelListAPI
            .request(withRequestPayload: ChannelListAPIRequestPayload())
            .trackActivity(app.activityIndicator)
            .subscribe(
                onNext: { [unowned self] (response: ChannelListAPIResponsePayload) in
                    self.channelList.value = response.channels
                },
                onError: { [unowned self] (error: Error) in
                    self.controllerNavigator.navigate.onNext({ (viewController: UIViewController) in
                        viewController.present(UIAlertController.alertController(forError: error, actions: nil), animated: true, completion: nil)
                    })
            })
            .addDisposableTo(disposeBag)
    }
}
