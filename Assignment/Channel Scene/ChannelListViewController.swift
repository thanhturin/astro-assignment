//
//  ChannelListViewController.swift
//  Assignment
//
//  Created by Thanh KFit on 3/5/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

final class ChannelListViewController: ViewController {

    @IBOutlet weak var tableView: UITableView!

    var viewModel: ChannelListViewControllerViewModel! {
        didSet {
            bind()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = ChannelListViewControllerViewModel()
        configureTableView()
        configureNavigationBar()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel.setAsActive()
    }

    func configureTableView() {
        tableView.tableFooterView = UIView()
        tableView.register(UINib(nibName: "ChannelTableViewCell", bundle: nil), forCellReuseIdentifier: "ChannelTableViewCell")
    }

    func configureNavigationBar() {

        navigationItem.title = "Channel"

        // Add Sort button
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Sort", style: UIBarButtonItemStyle.plain, target: self, action: #selector(didTapSortButton))
        navigationItem.rightBarButtonItem!.setTitleTextAttributes([NSFontAttributeName: UIFont.systemFont(ofSize: 15)], for: UIControlState())
        navigationItem.rightBarButtonItem?.tintColor = UIColor.red
    }

    func didTapSortButton() {
        let actionSheet = UIAlertController(title: "Sort By", message: "", preferredStyle: UIAlertControllerStyle.actionSheet)

        actionSheet.addAction(UIAlertAction(title: "Channel Name", style: UIAlertActionStyle.default, handler: { [weak self] _ in
            self?.viewModel.sortByChannelName.onNext()
        }))

        actionSheet.addAction(UIAlertAction(title: "Channel Number", style: UIAlertActionStyle.default, handler: { [weak self] _ in
            self?.viewModel.sortByChannelNumber.onNext()
        }))

        present(actionSheet, animated: true, completion: nil)
    }
}

extension ChannelListViewController: UITableViewDataSource {
    func numberOfSections(in _: UITableView) -> Int {
        return 1
    }

    func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
        return viewModel.channelList.value.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let channel = viewModel.channelList.value[indexPath.row]
        let cellViewModel = ChannelTableViewCellViewModel(channel: channel)
        let cell: ChannelTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ChannelTableViewCell", for: indexPath) as! ChannelTableViewCell
        cell.viewModel = cellViewModel
        return cell
    }
}

extension ChannelListViewController: UITableViewDelegate {
    func tableView(_: UITableView, heightForRowAt _: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }

    func tableView(_: UITableView, estimatedHeightForRowAt _: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}

// MARK: - ViewModelBinldable
extension ChannelListViewController: ViewModelBindable {
    func bind() {
        viewModel
            .controllerNavigator
            .navigate
            .filter { [weak self] _ -> Bool in
                guard let strongSelf = self else { return false }
                return strongSelf.viewModel.isActive
            }
            .subscribe(onNext: { [weak self] navigationClosure in
                if let strongSelf = self {
                    navigationClosure(strongSelf)
                }
            })
            .addDisposableTo(disposeBag)

        viewModel
            .channelList
            .asDriver()
            .drive(onNext: { [weak self] _ in
                self?.tableView.reloadData()
            })
            .addDisposableTo(disposeBag)
    }
}

// MARK: - Buildable
extension ChannelListViewController: Buildable {
    class func build(_ builder: ChannelListViewControllerViewModel) -> ChannelListViewController {
        let storyboard = UIStoryboard(name: "ChannelList", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ChannelListViewController") as! ChannelListViewController
        vc.viewModel = builder
        return vc
    }
}
