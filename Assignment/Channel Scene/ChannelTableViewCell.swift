//
//  ChannelTableViewCell.swift
//  Assignment
//
//  Created by Thanh KFit on 3/5/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

final class ChannelTableViewCell: UITableViewCell {

    @IBOutlet fileprivate weak var channelTitleLabel: UILabel!

    @IBOutlet fileprivate weak var channelNumberLabel: UILabel!

    @IBOutlet weak var favoriteButton: DOFavoriteButton!

    @IBAction func didTapFavoriteButton(_ sender: DOFavoriteButton) {
        if sender.isSelected {
            sender.deselect()
        } else {
            sender.select()
        }
        viewModel.updateFavoritedChannel.onNext()
    }

    var viewModel: ChannelTableViewCellViewModel! {
        didSet {
            bind()
        }
    }
}

extension ChannelTableViewCell: ViewModelBindable {
    func bind() {
        _ = viewModel.channelId.drive(channelNumberLabel.rx.text)
        _ = viewModel.channelTitle.drive(channelTitleLabel.rx.text)

        _ = viewModel.isFavoritedChannel
            .distinctUntilChanged()
            .driveNext { [unowned self] (isFavoritedChannel: Bool) in
                self.favoriteButton.isSelected = isFavoritedChannel
            }
    }
}
