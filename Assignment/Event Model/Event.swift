//
//  Event.swift
//  Assignment
//
//  Created by Thanh KFit on 3/7/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import Foundation
import RxSwift

final class Event {

    let eventID: String
    let channelId: Int
    let channelTitle: String
    let channelStbNumber: Int
    let programmeTitle: String
    let displayDateTimeUtc: Date
    let displayDateTime: Date
    let displayDuration: TimeInterval

    init(
        eventID: String
        , channelId: Int
        , channelTitle: String
        , channelStbNumber: Int
        , programmeTitle: String
        , displayDateTimeUtc: Date
        , displayDateTime: Date
        , displayDuration: TimeInterval
    ) {
        self.eventID = eventID
        self.channelId = channelId
        self.channelTitle = channelTitle
        self.channelStbNumber = channelStbNumber
        self.programmeTitle = programmeTitle
        self.displayDateTimeUtc = displayDateTimeUtc
        self.displayDateTime = displayDateTime
        self.displayDuration = displayDuration
    }
}

extension Event: Serializable {
    final class func serialize(_ jsonRepresentation: AnyObject?) -> Event? {
        guard let json = jsonRepresentation as? [String: AnyObject] else {
            return nil
        }

        // Required
        guard let eventID = json["eventID"] as? String else {
            return nil
        }

        guard let channelId = json["channelId"] as? Int else {
            return nil
        }

        guard let channelTitle = json["channelTitle"] as? String else {
            return nil
        }

        guard let channelStbNumber = {
            () -> Int? in
            guard let value = json["channelStbNumber"] as? String else {
                return nil
            }
            return Int(value)
        }() else {
            return nil
        }

        guard let programmeTitle = json["programmeTitle"] as? String else {
            return nil
        }

        guard let displayDateTimeUtc = {
            () -> Date? in
            guard let value = json["displayDateTimeUtc"] as? String else {
                return nil
            }
            return value.UTCDateTime
        }() else {
            return nil
        }

        guard let displayDateTime = {
            () -> Date? in
            guard let value = json["displayDateTime"] as? String else {
                return nil
            }
            return value.UTCDateTime
        }() else {
            return nil
        }

        guard let displayDuration = {
            () -> TimeInterval? in
            guard let value = json["displayDuration"] as? String else {
                return nil
            }
            return value.durationTime
        }() else {
            return nil
        }

        let result = Event(
            eventID: eventID
            , channelId: channelId
            , channelTitle: channelTitle
            , channelStbNumber: channelStbNumber
            , programmeTitle: programmeTitle
            , displayDateTimeUtc: displayDateTimeUtc
            , displayDateTime: displayDateTime
            , displayDuration: displayDuration
        )
        return result
    }
}
