//
//  EventsAPI.swift
//  Assignment
//
//  Created by Thanh KFit on 3/7/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire

struct EventsAPIRequestPayload {
    let channelId: [Int]
    let periodStart: String
    let periodEnd: String

    init(
        channelId: [Int]
        , periodStart: String
        , periodEnd: String
    ) {
        self.channelId = channelId
        self.periodStart = periodStart
        self.periodEnd = periodEnd
    }
}

extension EventsAPIRequestPayload: Deserializable {
    func deserialize() -> [String: AnyObject] {
        var parameters = [String: AnyObject]()

        parameters["channelId"] = channelId.map { "\($0)" }.joined(separator: ",") as AnyObject?
        parameters["periodStart"] = periodStart as AnyObject?
        parameters["periodEnd"] = periodEnd as AnyObject?

        return parameters
    }
}

protocol EventsAPI {
    func request(withRequestPayload requestPayload: EventsAPIRequestPayload) -> Observable<EventsAPIResponsePayload>
}

final class EventsAPIDefault: EventsAPI {

    private let apiService: APIService
    private let networkService: NetworkService

    init(apiService: APIService = apiServiceDefault
         , networkService: NetworkService = networkServiceDefault
    ) {
        self.apiService = apiService
        self.networkService = networkService
    }

    func request(withRequestPayload requestPayload: EventsAPIRequestPayload) -> Observable<EventsAPIResponsePayload> {

        let URL = apiService.baseURL.appendingPathComponent("/ams/v3/getEvents")

        let result = Observable.create {
            (observer: AnyObserver<EventsAPIResponsePayload>) -> Disposable in

            let parameters = requestPayload.deserialize()
            let urlRequest = try! self.networkService
                .urlRequestConvertible(method: .get, url: URL, parameters: parameters, encoding: URLEncoding.default, headers: nil)

            let request = self.networkService.managerWithDefaultConfiguration.request(urlRequest)

            request.responseObject {
                (response: DataResponse<EventsAPIResponsePayload>) -> Void in
                if let value = response.result.value {
                    observer.onNext(value)
                }
                if let error = response.result.error {
                    observer.onError(error)
                }
                observer.on(.completed)
            }
            return Disposables.create(with: { request.cancel() })
        }
        return result
    }
}

struct EventsAPIResponsePayload {
    // Array
    let events: [Event]

    init(events: [Event]) {
        self.events = events
    }
}

extension EventsAPIResponsePayload: Serializable {
    static func serialize(_ jsonRepresentation: AnyObject?) -> EventsAPIResponsePayload? {
        guard let json = jsonRepresentation as? [String: AnyObject] else {
            return nil
        }

        // Array
        var events = [Event]()
        if let items = json["getevent"] as? [AnyObject] {
            for item in items {
                if let event = Event.serialize(item) {
                    events.append(event)
                }
            }
        }

        let result = EventsAPIResponsePayload(
            events: events
        )

        return result
    }
}
