//
//  Buildable.swift
//  Assignment
//
//  Created by Thanh KFit on 3/5/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import Foundation

protocol Buildable {
    /// The type of the object to be built
    associatedtype ObjectType

    /// The type of the builder
    associatedtype BuilderType

    /// Build object using a builder
    static func build(_ builder: BuilderType) -> ObjectType
}
