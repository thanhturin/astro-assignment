//
//  ViewModelBindable.swift
//  Assignment
//
//  Created by Thanh KFit on 3/5/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import Foundation

protocol ViewModelBindable {
    associatedtype ViewModelType: ViewModel
    var viewModel: ViewModelType! { get set }
    func bind()
}
