//
//  ControllerNavigator.swift
//  Assignment
//
//  Created by Thanh KFit on 3/5/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

typealias SetViewControllerClosure = ((_ viewController: UIViewController) -> Void)
typealias NavigationClosure = ((_ viewController: UIViewController) -> Void)

protocol ControllerNavigator {
    var navigate: PublishSubject<NavigationClosure> { get }
    var setViewControllerClosure: PublishSubject<SetViewControllerClosure> { get }
}

final class ControllerNavigatorDefault: ControllerNavigator {
    let navigate = PublishSubject<NavigationClosure>()
    let setViewControllerClosure = PublishSubject<SetViewControllerClosure>()
}

let controllerNavigatorDefault = ControllerNavigatorDefault()
