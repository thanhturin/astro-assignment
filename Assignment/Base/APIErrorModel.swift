//
//  APIErrorModel.swift
//  Assignment
//
//  Created by Thanh KFit on 3/5/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire

class APIErrorModel: NSObject {

    let statusCode: Int
    let errors: [APIError]

    init(statusCode: Int, errors: [APIError]) {
        self.statusCode = statusCode
        self.errors = errors
    }

    var userVisibleErrorMessage: String {
        guard let error = errors.first
        else {
            return "API Error: \(statusCode)"
        }
        return error.message + "\nAPI Error: \(statusCode)"
    }
}

extension APIErrorModel: Serializable {
    static func serialize(_ jsonRepresentation: AnyObject?) -> APIErrorModel? {
        guard let json = jsonRepresentation as? [String: AnyObject] else {
            return nil
        }

        guard let statusCode = json["status"] as? Int else {
            return nil
        }
        var errors = [APIError]()

        if let items = json["errors"] as? [AnyObject] {
            for item in items {
                if let error = APIError.serialize(item) {
                    errors.append(error)
                }
            }
        }

        let result = APIErrorModel(statusCode: statusCode, errors: errors)
        return result
    }
}

final class APIError: NSObject {
    let attribute: String
    let message: String

    init(attribute: String, message: String) {
        self.attribute = attribute
        self.message = message
    }
}

extension APIError: Serializable {
    static func serialize(_ jsonRepresentation: AnyObject?) -> APIError? {
        guard let json = jsonRepresentation as? [String: AnyObject] else {
            return nil
        }

        guard let attribute = json["attribute"] as? String else {
            return nil
        }
        guard let message = json["message"] as? String else {
            return nil
        }

        let result = APIError(attribute: attribute, message: message)
        return result
    }
}

extension APIErrorModel {
    func toNSError() -> NSError {
        let userInfo: [AnyHashable: Any] = [NSLocalizedDescriptionKey: self.userVisibleErrorMessage]
        let error = NSError(domain: "Backend_Error", code: ErrorCode.apiError.code, userInfo: userInfo)
        return error
    }
}
