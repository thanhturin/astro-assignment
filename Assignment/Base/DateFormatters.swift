//
//  DateFormatters.swift
//  Assignment
//
//  Created by Thanh KFit on 3/7/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import Foundation

private let UTCDateFormat = "yyyy-MM-dd HH:mm:ss.S"
private let DateFormat = "yyyy-MM-dd"
private let TimeFormat = "HH:mm"

private let dateFormatterUnlocalized: DateFormatter = {
    let dateFormatterUnlocalized = DateFormatter()
    let enUSPOSIXLocale = Locale(identifier: "en_US_POSIX")
    dateFormatterUnlocalized.locale = enUSPOSIXLocale
    return dateFormatterUnlocalized
}()

private let dateFormatterLocalized: DateFormatter = {
    let dateFormatterLocalized = DateFormatter()
    return dateFormatterLocalized
}()

extension String {

    var durationTime: TimeInterval {
        var interval: Double = 0

        let parts = components(separatedBy: ":")
        for (index, part) in parts.reversed().enumerated() {
            interval += (Double(part) ?? 0) * pow(Double(60), Double(index))
        }

        return interval
    }

    var UTCDateTime: Date? {
        var result: Date?

        dateFormatterUnlocalized.dateFormat = UTCDateFormat
        result = dateFormatterUnlocalized.date(from: self)

        return result
    }
}

public extension Date {
    fileprivate var componentFlags: NSCalendar.Unit {
        return [.year, .month, .day, .weekOfMonth, .weekOfYear, .hour, .minute, .second, .weekday, .weekdayOrdinal]
    }

    static var currentCalendar: Calendar {
        return Calendar.autoupdatingCurrent
    }

    internal struct Seconds {
        static let minute = 60
        static let hour = minute * 60
        static let day = hour * 24
        static let week = day * 7
        //        static let year = day * 365...
        //            ...31556926 = 365.2422 days
    }

    var timeString: String {
        dateFormatterUnlocalized.dateFormat = TimeFormat
        let result = dateFormatterUnlocalized.string(from: self)
        return result
    }

    var UTCDateTimeString: String {
        dateFormatterUnlocalized.dateFormat = UTCDateFormat
        let result = dateFormatterUnlocalized.string(from: self)
        return result
    }

    // MARK: Days
    static func daysFromNow(_ days: Int) -> Date {
        return Date().addDays(days)
    }

    static func daysBeforeNow(_ days: Int) -> Date {
        return Date().subtractDays(days)
    }

    static func now() -> Date {
        return Date()
    }

    static func today() -> Date {
        return Date.now()
    }

    static func tomorrow() -> Date {
        return Date.daysFromNow(1)
    }

    static func yesterday() -> Date {
        return Date.daysBeforeNow(1)
    }

    // MARK: Hours
    static func hoursFromNow(_ hours: Int) -> Date {
        let timeInterval = Date().timeIntervalSinceReferenceDate + Double(Seconds.hour * hours)
        return Date(timeIntervalSinceReferenceDate: timeInterval)
    }

    static func hoursBeforeNow(_ hours: Int) -> Date {
        let timeInterval = Date().timeIntervalSinceReferenceDate - Double(Seconds.hour * hours)
        return Date(timeIntervalSinceReferenceDate: timeInterval)
    }

    // MARK: Minutes
    static func minutesFromNow(_ minutes: Int) -> Date {
        let timeInterval = Date().timeIntervalSinceReferenceDate + Double(Seconds.minute * minutes)
        return Date(timeIntervalSinceReferenceDate: timeInterval)
    }

    static func minutesBeforeNow(_ minutes: Int) -> Date {
        let timeInterval = Date().timeIntervalSinceReferenceDate - Double(Seconds.minute * minutes)
        return Date(timeIntervalSinceReferenceDate: timeInterval)
    }

    // MARK: - String Properties
    // MARK: Format and Style
    func stringWithFormat(_ format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: self)
    }

    func stringWithDateStyle(_ dateStyle: DateFormatter.Style, timeStyle: DateFormatter.Style) -> String {
        let formatter = DateFormatter()
        formatter.dateStyle = dateStyle
        formatter.timeStyle = timeStyle
        return formatter.string(from: self)
    }

    // MARK: Short Formats
    func shortString() -> String {
        return stringWithDateStyle(.short, timeStyle: .short)
    }

    func shortTimeString() -> String {
        return stringWithDateStyle(.none, timeStyle: .short)
    }

    func shortDateString() -> String {
        return stringWithDateStyle(.short, timeStyle: .none)
    }

    // MARK: Medium Formats
    func mediumString() -> String {
        return stringWithDateStyle(.medium, timeStyle: .medium)
    }

    func mediumTimeString() -> String {
        return stringWithDateStyle(.none, timeStyle: .medium)
    }

    func mediumDateString() -> String {
        return stringWithDateStyle(.medium, timeStyle: .none)
    }

    // MARK: Long Formats
    func longString() -> String {
        return stringWithDateStyle(.long, timeStyle: .long)
    }

    func longTimeString() -> String {
        return stringWithDateStyle(.none, timeStyle: .long)
    }

    func longDateString() -> String {
        return stringWithDateStyle(.long, timeStyle: .none)
    }

    // MARK: - Comparing Dates
    // MARK: Equal Dates
    func equalToDateIgnoringTime(_ date: Date) -> Bool {
        let components1 = (Date.currentCalendar as NSCalendar).components(componentFlags, from: self)
        let components2 = (Date.currentCalendar as NSCalendar).components(componentFlags, from: date)
        return ((components1.day == components2.day) && (components1.month == components2.month) && (components1.year == components2.year))
    }

    // MARK: Days
    func today() -> Bool {
        return equalToDateIgnoringTime(Date())
    }

    func tomorrow() -> Bool {
        return equalToDateIgnoringTime(Date.tomorrow())
    }

    func yesterday() -> Bool {
        return equalToDateIgnoringTime(Date.yesterday())
    }

    // MARK: Weeks
    func sameWeekAsDate(_ date: Date) -> Bool {
        let components1 = (Date.currentCalendar as NSCalendar).components(componentFlags, from: self)
        let components2 = (Date.currentCalendar as NSCalendar).components(componentFlags, from: date)
        if components1.weekOfYear != components2.weekOfYear { return false }
        return (fabs(timeIntervalSince(date)) < Double(Seconds.week))
    }

    func thisWeek() -> Bool {
        return sameWeekAsDate(Date())
    }

    func nextWeek() -> Bool {
        let timeInterval = Date().timeIntervalSinceReferenceDate + Double(Seconds.week)
        let date = Date(timeIntervalSinceReferenceDate: timeInterval)
        return sameWeekAsDate(date)
    }

    func lastWeek() -> Bool {
        let timeInterval = Date().timeIntervalSinceReferenceDate - Double(Seconds.week)
        let date = Date(timeIntervalSinceReferenceDate: timeInterval)
        return sameWeekAsDate(date)
    }

    // MARK: Months
    func sameMonthAsDate(_ date: Date) -> Bool {
        let components1 = (Date.currentCalendar as NSCalendar).components([.year, .month], from: self)
        let components2 = (Date.currentCalendar as NSCalendar).components([.year, .month], from: date)
        return ((components1.month == components2.month) && (components1.year == components2.year))
    }

    func thisMonth() -> Bool {
        return sameMonthAsDate(Date())
    }

    func lastMonth() -> Bool {
        return sameMonthAsDate(Date().subtractMonths(1))
    }

    func nextMonth() -> Bool {
        return sameMonthAsDate(Date().addMonths(1))
    }

    // MARK: Years
    func sameYearAsDate(_ date: Date) -> Bool {
        let components1 = (Date.currentCalendar as NSCalendar).components(.year, from: self)
        let components2 = (Date.currentCalendar as NSCalendar).components(.year, from: date)
        return (components1.year == components2.year)
    }

    func thisYear() -> Bool {
        return sameYearAsDate(Date())
    }

    func nextYear() -> Bool {
        let components1 = (Date.currentCalendar as NSCalendar).components(.year, from: self)
        let components2 = (Date.currentCalendar as NSCalendar).components(.year, from: Date())
        return (components1.year! == (components2.year! + 1))
    }

    func lastYear() -> Bool {
        let components1 = (Date.currentCalendar as NSCalendar).components(.year, from: self)
        let components2 = (Date.currentCalendar as NSCalendar).components(.year, from: Date())
        return (components1.year! == (components2.year! - 1))
    }

    // MARK: Relativity
    func earlierThanDate(_ date: Date) -> Bool {
        return (compare(date) == .orderedAscending)
    }

    func laterThanDate(_ date: Date) -> Bool {
        return (compare(date) == .orderedDescending)
    }

    func inTheFuture() -> Bool {
        return laterThanDate(Date())
    }

    func inThePast() -> Bool {
        return earlierThanDate(Date())
    }

    // MARK: - Roles
    func typicallyWeekend() -> Bool {
        let components = (Date.currentCalendar as NSCalendar).components(.weekday, from: self)
        return ((components.weekday == 1) || (components.weekday == 7))
    }

    func typicallyWeekday() -> Bool {
        return !typicallyWeekend()
    }

    // MARK: - Adjusting Dates
    // MARK: Years
    func addYears(_ years: Int) -> Date {
        var dateComponents = DateComponents()
        dateComponents.year = years
        return (Calendar.current as NSCalendar).date(byAdding: dateComponents, to: self, options: .wrapComponents)!
    }

    func subtractYears(_ years: Int) -> Date {
        return addYears(-years)
    }

    // MARK: Months
    func addMonths(_ months: Int) -> Date {
        var dateComponents = DateComponents()
        dateComponents.month = months
        return (Calendar.current as NSCalendar).date(byAdding: dateComponents, to: self, options: .wrapComponents)!
    }

    func subtractMonths(_ months: Int) -> Date {
        return addMonths(-months)
    }

    // MARK: Days
    func addDays(_ days: Int) -> Date {
        var dateComponents = DateComponents()
        dateComponents.day = days
        return (Calendar.current as NSCalendar).date(byAdding: dateComponents, to: self, options: .wrapComponents)!
    }

    func subtractDays(_ days: Int) -> Date {
        return addDays(-days)
    }

    // MARK: Hours
    func addHours(_ hours: Int) -> Date {
        let timeInterval = timeIntervalSinceReferenceDate + Double(Seconds.hour * hours)
        return Date(timeIntervalSinceReferenceDate: timeInterval)
    }

    func subtractHours(_ hours: Int) -> Date {
        return addHours(-hours)
    }

    // MARK: Minutes
    func addMinutes(_ minutes: Int) -> Date {
        let timeInterval = timeIntervalSinceReferenceDate + Double(Seconds.minute * minutes)
        return Date(timeIntervalSinceReferenceDate: timeInterval)
    }

    func subtractMinutes(_ minutes: Int) -> Date {
        return addMinutes(-minutes)
    }

    // MARK: Relative Components
    func componentsWithOffsetToDate(_ date: Date) -> DateComponents {
        return (Date.currentCalendar as NSCalendar).components(componentFlags, from: self, to: date, options: .wrapComponents)
    }

    func componentsWithOffsetFromDate(_ date: Date) -> DateComponents {
        return (Date.currentCalendar as NSCalendar).components(componentFlags, from: date, to: self, options: .wrapComponents)
    }

    func daysFromNow() -> Int {
        let components = componentsWithOffsetFromDate(Date())
        return components.day!
    }

    // MARK: - Extremes
    func startOfDay() -> Date {
        var components = (Date.currentCalendar as NSCalendar).components(componentFlags, from: self)
        components.hour = 0
        components.minute = 0
        components.second = 0
        return Date.currentCalendar.date(from: components)!
    }

    func endOfDay() -> Date {
        var components = (Date.currentCalendar as NSCalendar).components(componentFlags, from: self)
        components.hour = 23
        components.minute = 59
        components.second = 59
        return Date.currentCalendar.date(from: components)!
    }

    // MARK: - Intervals
    // MARK: Minutes
    func minutesAfterDate(_ date: Date) -> Int {
        let timeInterval = timeIntervalSince(date)
        return Int(timeInterval / Double(Seconds.minute))
    }

    func minutesBeforeDate(_ date: Date) -> Int {
        let timeInterval = date.timeIntervalSince(self)
        return Int(timeInterval / Double(Seconds.minute))
    }

    // MARK: Hours
    func hoursAfterDate(_ date: Date) -> Int {
        let timeInterval = timeIntervalSince(date)
        return Int(timeInterval / Double(Seconds.hour))
    }

    func hoursBeforeDate(_ date: Date) -> Int {
        let timeInterval = date.timeIntervalSince(self)
        return Int(timeInterval / Double(Seconds.hour))
    }

    // MARK: Days
    func daysAfterDate(_ date: Date) -> Int {
        let timeInterval = timeIntervalSince(date)
        return Int(timeInterval / Double(Seconds.day))
    }

    func daysBeforeDay(_ date: Date) -> Int {
        let timeInterval = date.timeIntervalSince(self)
        return Int(timeInterval / Double(Seconds.day))
    }

    // MARK: - Component Properties
    var nearestHour: Int {
        let timeInterval: TimeInterval = Date().timeIntervalSinceReferenceDate + Double(Seconds.minute * 30)
        let date = Date(timeIntervalSinceReferenceDate: timeInterval)
        let components = (Date.currentCalendar as NSCalendar).components(componentFlags, from: date)
        return components.hour!
    }
}
