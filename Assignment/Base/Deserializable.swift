//
//  Deserializable.swift
//  Assignment
//
//  Created by Thanh KFit on 3/5/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import Foundation

public protocol Deserializable {
    /**
     Deserialize object to it's JSON representation

     - returns: JSON representation of the object it's applied to
     */
    func deserialize() -> [String: AnyObject]
}
