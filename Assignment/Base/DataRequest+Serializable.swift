//
//  DataRequest+Serializable.swift
//  Assignment
//
//  Created by Thanh KFit on 3/5/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import Foundation
import Alamofire
import RxSwift

extension DataRequest {
    /**
     Serilize the json rescived from the network to the object represented by the function genaric type.
     The genaric type must comfirm to Serializable Protocol

     - parameter keyPath: The path insde the json the object to be serialized should be found at. Send nil to start the serialization from the top of the rescived json.

     - returns: The serialized object if the serialization was successful. Otherwise, returns an Error.
     */
    public static func ObjectMapperSerializer<T: Serializable>(_ keyPath: String?) -> DataResponseSerializer<T> {
        return DataResponseSerializer { request, response, data, error in
            if let error = error {
                return .failure(error)
            }

            guard let _ = data else {
                let error = ResponseError.noDataReceived(request: request, statusCode: response?.statusCode)
                return .failure(error)
            }

            let jsonResponseSerializer = DataRequest.jsonResponseSerializer(options: .allowFragments)
            let result = jsonResponseSerializer.serializeResponse(request, response, data, error)

            if let statusCode = response?.statusCode, statusCode >= 400 {

                if let apiError = APIErrorModel.serialize(result.value as AnyObject?) {
                    return .failure(apiError.toNSError())
                } else {
                    return .failure(ResponseError.backEndError(request: request, statusCode: statusCode, errorResponse: result.value as AnyObject?))
                }
            }

            let JSONToSerialize: Any?
            if let keyPath = keyPath, keyPath.isEmpty == false {
                JSONToSerialize = (result.value as AnyObject?)?.value(forKeyPath: keyPath)
            } else {
                JSONToSerialize = result.value
            }

            if let parsedObject = T.serialize(JSONToSerialize as AnyObject?) {
                return .success(parsedObject as! T)
            } else {
                return .failure(ResponseError.dataSerializationFailed(response: JSONToSerialize as AnyObject?))
            }
        }
    }

    /**
     Adds a handler to be called once the request has finished.

     - parameter queue:             The queue on which the completion handler is dispatched.
     - parameter keyPath:           The key path where object mapping should be performed
     - parameter object:            An object to perform the mapping on to
     - parameter completionHandler: A closure to be executed once the request has finished and the data has been mapped by ObjectMapper.

     - returns: The request.
     */
    @discardableResult
    public func responseObject<T: Serializable>(queue: DispatchQueue? = nil, keyPath: String? = nil, completionHandler: @escaping (DataResponse<T>) -> Void) -> Self {
        return response(queue: queue, responseSerializer: DataRequest.ObjectMapperSerializer(keyPath), completionHandler: completionHandler)
    }
}
