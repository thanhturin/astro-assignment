//
//  App.swift
//  Assignment
//
//  Created by Thanh KFit on 3/5/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

protocol AppType {
    var activityIndicator: ActivityIndicator { get }
}

final class App: AppType {
    let activityIndicator = ActivityIndicator()
    var disposeBag = DisposeBag()

    init() {
        activityIndicator
            .asDriver()
            .drive(UIApplication.shared.rx.isNetworkActivityIndicatorVisible)
            .addDisposableTo(disposeBag)

        activityIndicator
            .skip(1) // The first value is the initiation value
            .throttle(0.7)
            .driveNext { (active: Bool) -> Void in
                guard UIApplication.shared.keyWindow != nil else {
                    return
                }
                if active {
                    SwiftLoader.show(animated: true) }
                else {
                    SwiftLoader.hide()
                }
            }
            .addDisposableTo(disposeBag)
    }
}
