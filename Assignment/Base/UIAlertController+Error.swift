//
//  UIAlertController+Error.swift
//  Assignment
//
//  Created by Thanh KFit on 3/5/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import Foundation
import UIKit

extension UIAlertController {
    final class func alertController(forError error: Error, actions: [UIAlertAction]? = nil) -> UIAlertController {
        let errorDescription = { () -> String in
            if (error as NSError).localizedDescription != "" {
                let errorDescription = (error as NSError).localizedDescription
                return errorDescription
            }
            return errorMessengerDefault
        }()

        let errorString = "Fave"
        return alertController(forTitle: errorString, message: errorDescription, preferredStyle: .alert, actions: actions)
    }

    final class func alertController(forTitle title: String, message: String, preferredStyle: UIAlertControllerStyle = .alert, actions: [UIAlertAction]? = nil) -> UIAlertController {

        let actionsToAdd: [UIAlertAction] = {
            if actions != nil {
                return actions!
            } else {
                let okAction = UIAlertAction(title: NSLocalizedString("msg_dialog_ok", comment: ""), style: .cancel, handler: nil)
                return [okAction]
            }
        }()

        let alertController = UIAlertController(title: title, message: message, preferredStyle: preferredStyle)
        for action in actionsToAdd {
            alertController.addAction(action)
        }

        return alertController
    }
}

extension UIAlertController {
    open override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.portrait
    }

    open override var shouldAutorotate: Bool {
        return false
    }
}
