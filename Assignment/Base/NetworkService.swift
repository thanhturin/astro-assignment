//
//  NetworkService.swift
//  Assignment
//
//  Created by Thanh KFit on 3/5/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import Foundation
import Alamofire

protocol NetworkService {

    var managerWithDefaultConfiguration: SessionManager { get }

    func urlRequestConvertible(
        method: HTTPMethod
        , url: URL
        , parameters: [String: AnyObject]?
        , encoding: ParameterEncoding
        , headers: [String: String]?
    )
        throws -> URLRequestConvertible
}

final class NetworkServiceDefault: NetworkService {

    let managerWithDefaultConfiguration: SessionManager

    var cookie: String?
    init() {
        let userAgent: String = {
            let infoDict = Bundle.main.infoDictionary
            let appVersion = infoDict!["CFBundleShortVersionString"]!
            let buildNumber = infoDict!["CFBundleVersion"]!
            let currentDeviceModel = UIDevice.current.model
            let currentDeviceSystemVersion = UIDevice.current.systemVersion
            let userAgent = "Assignment/v\(appVersion)-\(buildNumber) (\(currentDeviceModel);iOS \(currentDeviceSystemVersion))"

            return userAgent
        }()

        managerWithDefaultConfiguration = {
            let configuration = URLSessionConfiguration.default
            configuration.httpAdditionalHeaders = ["User-Agent": userAgent]
            return SessionManager(configuration: configuration)
        }()
    }

    func urlRequestConvertible(
        method: HTTPMethod
        , url: URL
        , parameters: [String: AnyObject]? = nil
        , encoding: ParameterEncoding = URLEncoding.default
        , headers: [String: String]? = nil
    ) throws -> URLRequestConvertible {

        var urlRequest = Foundation.URLRequest(url: url)
        urlRequest.httpMethod = method.rawValue

        if let headers = headers {
            for (headerField, headerValue) in headers {
                urlRequest.setValue(headerValue, forHTTPHeaderField: headerField)
            }
        }

        if let cookie = cookie {
            urlRequest.setValue(cookie, forHTTPHeaderField: "Cookie")
        }

        if let parameters = parameters {
            urlRequest = try encoding.encode(urlRequest, with: parameters)
        }

        #if DEBUG
            urlRequest.cachePolicy = Foundation.URLRequest.CachePolicy.reloadIgnoringCacheData
        #endif

        return URLRequestConvertibleDummyStruct(urlRequest: urlRequest)
    }

    private struct URLRequestConvertibleDummyStruct: URLRequestConvertible {
        let urlRequest: URLRequest
        init(urlRequest: URLRequest) {
            self.urlRequest = urlRequest
        }

        func asURLRequest() throws -> URLRequest {
            return urlRequest
        }
    }
}
