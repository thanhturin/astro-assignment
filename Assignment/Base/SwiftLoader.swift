//
//  BSLoader.swift
//  Brainstorage
//
//  Created by Kirill Kunst on 07.02.15.
//  Copyright (c) 2015 Kirill Kunst. All rights reserved.
//
import UIKit
import QuartzCore
import CoreGraphics

let loaderSpinnerMarginSide: CGFloat = 35.0
let loaderSpinnerMarginTop: CGFloat = 20.0
let loaderTitleMargin: CGFloat = 5.0

public class SwiftLoader: UIView {

    private var coverView: UIView?
    private var titleLabel: UILabel?
    private var loadingView: SwiftLoadingView?
    private var animated: Bool = true
    private var canUpdated = false
    private var title: String?
    private var speed = 1

    private var config: Config = Config() {
        didSet {
            self.loadingView?.config = config
        }
    }

    func rotated(notification _: NSNotification) {

        let loader = SwiftLoader.sharedInstance

        let height: CGFloat = UIScreen.main.bounds.size.height
        let width: CGFloat = UIScreen.main.bounds.size.width
        let center: CGPoint = CGPoint(x: width / 2.0, y: height / 2.0)

        loader.center = center
        loader.coverView?.frame = UIScreen.main.bounds
    }

    public override var frame: CGRect {
        didSet {
            update()
        }
    }

    class var sharedInstance: SwiftLoader {
        struct Singleton {
            static let instance = SwiftLoader(frame: CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: Config().size, height: Config().size)))
        }
        return Singleton.instance
    }

    public class func show(animated: Bool) {
        show(title: nil, animated: animated)
    }

    public class func show(title: String?, animated: Bool) {

        let currentWindow: UIWindow = UIApplication.shared.keyWindow!

        let loader = SwiftLoader.sharedInstance
        loader.canUpdated = true
        loader.animated = animated
        loader.title = title
        loader.update()

        NotificationCenter.default.addObserver(loader, selector: #selector(loader.rotated(notification:)),
                                               name: NSNotification.Name.UIDeviceOrientationDidChange,
                                               object: nil)

        let height: CGFloat = UIScreen.main.bounds.size.height
        let width: CGFloat = UIScreen.main.bounds.size.width
        let center: CGPoint = CGPoint(x: width / 2.0, y: height / 2.0)

        loader.center = center

        if loader.superview == nil {
            loader.coverView = UIView(frame: currentWindow.bounds)
            loader.coverView?.backgroundColor = loader.config.foregroundColor.withAlphaComponent(loader.config.foregroundAlpha)

            currentWindow.addSubview(loader.coverView!)
            currentWindow.addSubview(loader)
            loader.start()
        }
    }

    public class func hide() {

        let loader = SwiftLoader.sharedInstance
        NotificationCenter.default.removeObserver(loader)

        loader.stop()
    }

    public class func setConfig(config: Config) {
        let loader = SwiftLoader.sharedInstance
        loader.config = config
        loader.frame = CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: loader.config.size, height: loader.config.size))
    }

    /**
     Private methods
     */

    private func setup() {
        alpha = 0
        update()
    }

    private func start() {
        loadingView?.start()

        if animated {
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                self.alpha = 1
            }, completion: { (_) -> Void in

            })
        } else {
            alpha = 1
        }
    }

    private func stop() {

        if animated {
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                self.alpha = 0
            }, completion: { (_) -> Void in
                self.removeFromSuperview()
                self.coverView?.removeFromSuperview()
                self.loadingView?.stop()
            })
        } else {
            alpha = 0
            removeFromSuperview()
            coverView?.removeFromSuperview()
            loadingView?.stop()
        }
    }

    private func update() {
        backgroundColor = config.backgroundColor
        layer.cornerRadius = config.cornerRadius
        let loadingViewSize = frame.size.width - (loaderSpinnerMarginSide * 2)

        if loadingView == nil {
            loadingView = SwiftLoadingView(frame: frameForSpinner())
            addSubview(loadingView!)
        } else {
            loadingView?.frame = frameForSpinner()
        }

        if titleLabel == nil {
            titleLabel = UILabel(frame: CGRect(origin: CGPoint(x: loaderTitleMargin, y: loaderSpinnerMarginTop + loadingViewSize), size: CGSize(width: frame.width - loaderTitleMargin * 2, height: 42.0)))
            addSubview(titleLabel!)
            titleLabel?.numberOfLines = 1
            titleLabel?.textAlignment = NSTextAlignment.center
            titleLabel?.adjustsFontSizeToFitWidth = true
        } else {
            titleLabel?.frame = CGRect(origin: CGPoint(x: loaderTitleMargin, y: loaderSpinnerMarginTop + loadingViewSize), size: CGSize(width: frame.width - loaderTitleMargin * 2, height: 42.0))
        }

        titleLabel?.font = config.titleTextFont
        titleLabel?.textColor = config.titleTextColor
        titleLabel?.text = title

        titleLabel?.isHidden = title == nil
    }

    func frameForSpinner() -> CGRect {
        let loadingViewSize = frame.size.width - (loaderSpinnerMarginSide * 2)

        if title == nil {
            let yOffset = (frame.size.height - loadingViewSize) / 2
            return CGRect(origin: CGPoint(x: loaderSpinnerMarginSide, y: yOffset), size: CGSize(width: loadingViewSize, height: loadingViewSize))
        }
        return CGRect(origin: CGPoint(x: loaderSpinnerMarginSide, y: loaderSpinnerMarginTop), size: CGSize(width: loadingViewSize, height: loadingViewSize))
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    /**
     *  Loader View
     */
    class SwiftLoadingView: UIView {

        private var speed: Int?
        private var lineWidth: Float?
        private var lineTintColor: UIColor?
        private var backgroundLayer: CAShapeLayer?
        private var isSpinning: Bool?

        var config: Config = Config() {
            didSet {
                self.update()
            }
        }

        override init(frame: CGRect) {
            super.init(frame: frame)
            setup()
        }

        required init?(coder aDecoder: NSCoder) {
            super.init(coder: aDecoder)
        }

        /**
         Setup loading view
         */

        func setup() {
            backgroundColor = UIColor.clear
            lineWidth = fmaxf(Float(frame.size.width) * 0.025, 1)

            backgroundLayer = CAShapeLayer()
            backgroundLayer?.strokeColor = config.spinnerColor.cgColor
            backgroundLayer?.fillColor = backgroundColor?.cgColor
            backgroundLayer?.lineCap = kCALineCapRound
            backgroundLayer?.lineWidth = CGFloat(lineWidth!)
            layer.addSublayer(backgroundLayer!)
        }

        func update() {
            lineWidth = config.spinnerLineWidth
            speed = config.speed

            backgroundLayer?.lineWidth = CGFloat(lineWidth!)
            backgroundLayer?.strokeColor = config.spinnerColor.cgColor
        }

        /**
         Draw Circle
         */

        override func draw(_: CGRect) {
            backgroundLayer?.frame = bounds
        }

        func drawBackgroundCircle(partial: Bool) {
            let startAngle: CGFloat = CGFloat(M_PI) / CGFloat(2.0)
            var endAngle: CGFloat = (2.0 * CGFloat(M_PI)) + startAngle

            let center: CGPoint = CGPoint(x: bounds.size.width / 2, y: bounds.size.height / 2)
            let radius: CGFloat = (CGFloat(bounds.size.width) - CGFloat(lineWidth!)) / CGFloat(2.0)

            let processBackgroundPath: UIBezierPath = UIBezierPath()
            processBackgroundPath.lineWidth = CGFloat(lineWidth!)

            if partial {
                endAngle = (1.8 * CGFloat(M_PI)) + startAngle
            }

            processBackgroundPath.addArc(withCenter: center, radius: radius, startAngle: startAngle, endAngle: endAngle, clockwise: true)
            backgroundLayer?.path = processBackgroundPath.cgPath
        }

        /**
         Start and stop spinning
         */

        func start() {
            isSpinning? = true
            drawBackgroundCircle(partial: true)

            let rotationAnimation: CABasicAnimation = CABasicAnimation(keyPath: "transform.rotation.z")
            rotationAnimation.toValue = NSNumber(value: M_PI * 2.0)
            rotationAnimation.duration = 1
            rotationAnimation.isCumulative = true
            rotationAnimation.repeatCount = HUGE
            backgroundLayer?.add(rotationAnimation, forKey: "rotationAnimation")
        }

        func stop() {
            drawBackgroundCircle(partial: false)

            backgroundLayer?.removeAllAnimations()
            isSpinning? = false
        }
    }

    /**
     * Loader config
     */
    public struct Config {

        /**
         *  Size of loader
         */
        public var size: CGFloat = 120.0

        /**
         *  Color of spinner view
         */
        public var spinnerColor = UIColor.black

        /**
         *  S
         */
        public var spinnerLineWidth: Float = 1.0

        /**
         *  Color of title text
         */
        public var titleTextColor = UIColor.black

        /**
         *  Speed of the spinner
         */
        public var speed: Int = 1

        /**
         *  Font for title text in loader
         */
        public var titleTextFont: UIFont = UIFont.boldSystemFont(ofSize: 16.0)

        /**
         *  Background color for loader
         */
        public var backgroundColor = UIColor.white

        /**
         *  Foreground color
         */
        public var foregroundColor = UIColor.clear

        /**
         *  Foreground alpha CGFloat, between 0.0 and 1.0
         */
        public var foregroundAlpha: CGFloat = 0.0

        /**
         *  Corner radius for loader
         */
        public var cornerRadius: CGFloat = 10.0

        public init() {}
    }
}
