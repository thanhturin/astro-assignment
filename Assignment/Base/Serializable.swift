//
//  Serializable.swift
//  Assignment
//
//  Created by Thanh KFit on 3/5/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import Foundation

public protocol Serializable {
    associatedtype SerializableType
    static func serialize(_ jsonRepresentation: AnyObject?) -> SerializableType?
}
