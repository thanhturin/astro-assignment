//
//  DescribableError.swift
//  Assignment
//
//  Created by Thanh KFit on 3/5/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import Foundation

protocol DescribableError: Error {
    var description: String { get }
    var userVisibleDescription: String { get }
}

extension NSError {
    func toDescribableError() -> DescribableError {
        struct DescribableErrorStruct: DescribableError {
            let description: String
            let userVisibleDescription: String
            init(description: String, userVisibleDescription: String) {
                self.description = description
                self.userVisibleDescription = userVisibleDescription
            }
        }
        var description: String = ""
        if let localDescription = self.userInfo[NSLocalizedDescriptionKey] {
            description = "\(localDescription)"
        }

        let describableError = DescribableErrorStruct(
            description: description
            , userVisibleDescription: NSLocalizedString("msg_something_wrong", comment: "")
        )
        return describableError
    }
}

extension DescribableError {
    func toNSError() -> NSError {
        let userInfo: [AnyHashable: Any] = [NSLocalizedDescriptionKey: self.userVisibleDescription]
        let error = NSError(domain: "", code: 0, userInfo: userInfo)
        return error
    }
}
