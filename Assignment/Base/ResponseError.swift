//
//  ResponseError.swift
//  Assignment
//
//  Created by Thanh KFit on 3/5/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import Foundation
import Alamofire

enum ResponseError: DescribableError {
    case noDataReceived(request: URLRequest?, statusCode: Int?)
    case backEndError(request: URLRequest?, statusCode: Int, errorResponse: AnyObject?)
    case dataSerializationFailed(response: AnyObject?)

    var description: String {
        switch self {
        case let .noDataReceived(request, statusCode):
            var result = ""
            result = result + "Response: No Data received\n"
            result = result + "Request URL: \(request?.url?.absoluteString)\n"
            result = result + "Status code: \(statusCode)\n"
            if let headerFields = request?.allHTTPHeaderFields {
                result = result + "Headers: \(request?.allHTTPHeaderFields)\n"

                if let cookie = headerFields["Set-Cookie"] {
                    result = result + "Cookie: \(cookie)\n"
                } else {
                }
            } else {
                result = result + "Headers: No headers\n"
                result = result + "Cookie: No Cookie\n"
            }

            return result

        case let .backEndError(request, statusCode, errorResponse):
            var result = ""
            result = result + "Response: \(errorResponse)\n"
            result = result + "Request URL: \(request?.url?.absoluteString)\n"
            result = result + "Status code: \(statusCode)\n"
            if let headerFields = request?.allHTTPHeaderFields {
                result = result + "Headers: \(request?.allHTTPHeaderFields)\n"

                if let cookie = headerFields["Set-Cookie"] {
                    result = result + "Cookie: \(cookie)\n"
                } else {
                }
            } else {
                result = result + "Headers: No headers\n"
                result = result + "Cookie: No Cookie\n"
            }

            return result

        case let .dataSerializationFailed(response):
            var result = ""
            result = result + "Response: \(response)"
            return result
        }
    }

    var userVisibleDescription: String {
        switch self {
        case .noDataReceived:
            return "Can not receive any data"
        case .backEndError:
            return "Something went wrong! Please try again."
        case .dataSerializationFailed:
            return "Failed to serialize response."
        }
    }
}
