//
//  APIService.swift
//  Assignment
//
//  Created by Thanh KFit on 3/5/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import Foundation

protocol APIService {
    var baseURL: URL { get }
}

final class APIServiceDefault: APIService {

    var baseURL: URL {
        return URL(string: "http://ams-api.astro.com.my")!
    }

    init() {
    }
}
