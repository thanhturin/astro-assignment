//
//  ShareInstances.swift
//  Assignment
//
//  Created by Thanh KFit on 3/5/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import Foundation
import UIKit

let errorMessengerDefault = "Something went wrong. Please try again!"
let astroPinkColor = UIColor(red: 228.0 / 255.0, green: 40.0 / 255.0, blue: 125.0 / 255.0, alpha: 1.0)

let favoritedChannelServiceDefault = FavoritedChannelServiceDefault()
let appDefault = App()
let apiServiceDefault = APIServiceDefault()
let networkServiceDefault = NetworkServiceDefault()
