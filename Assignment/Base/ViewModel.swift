//
//  ViewModel.swift
//  Assignment
//
//  Created by Thanh KFit on 3/5/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import Foundation
import RxSwift

class ViewModel: NSObject {
    var disposeBag = DisposeBag()

    fileprivate static var activeViewModelUuid: String = Foundation.UUID().uuidString
    func setAsActive() {
        ViewModel.activeViewModelUuid = UUID
    }

    var isActive: Bool {
        return ViewModel.activeViewModelUuid == UUID
    }

    let UUID = Foundation.UUID().uuidString

    let controllerNavigator: ControllerNavigator
    let app: AppType

    init(
        controllerNavigator: ControllerNavigator = controllerNavigatorDefault
        , app: AppType = appDefault
    ) {
        self.app = app
        self.controllerNavigator = controllerNavigator
        super.init()
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}
