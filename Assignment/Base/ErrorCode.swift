//
//  ErrorCode.swift
//  Assignment
//
//  Created by Thanh KFit on 3/5/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import Foundation

enum ErrorCode: String {
    case apiError = "00102"
    case serializationError = "00103"

    var code: Int {
        switch self {
        case .apiError:
            return 00102
        case .serializationError:
            return 00103
        }
    }
}
