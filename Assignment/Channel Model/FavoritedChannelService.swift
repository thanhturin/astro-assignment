//
//  FavoritedChannelService.swift
//  Assignment
//
//  Created by Thanh KFit on 3/5/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

protocol FavoritedChannelService {
    var favoritedChannels: Variable<[Int]> { get }
    func loadFavoritedChannelsCache() -> [Int]
    func clearFavoritedChannelsCache()
    func updateFavoritedChannel(channelId: Int)
    func cacheFavoritedChannels(chanelsId: [Int])
}

class FavoritedChannelServiceDefault: FavoritedChannelService {
    let favoritedChannels = Variable([Int]())

    init() {
        favoritedChannels.value = loadFavoritedChannelsCache()
    }

    internal func cacheFavoritedChannels(chanelsId: [Int]) {
        let data = NSKeyedArchiver.archivedData(withRootObject: chanelsId)
        UserDefaults.standard
            .set(
                data, forKey: "FavoritedChannelService.favoritedChannels")
    }

    internal func updateFavoritedChannel(channelId: Int) {
        var favoritedChannels = loadFavoritedChannelsCache()

        let channelElements = favoritedChannels.filter { (_channelId: Int) -> Bool in
            _channelId == channelId
        }

        if channelElements.isEmpty {
            favoritedChannels.append(channelId)
        } else {
            favoritedChannels = favoritedChannels.filter({ (_channelId: Int) -> Bool in
                _channelId != channelId
            })
        }

        self.favoritedChannels.value = favoritedChannels
        cacheFavoritedChannels(chanelsId: favoritedChannels)
    }

    internal func loadFavoritedChannelsCache() -> [Int] {
        guard let cacheData = UserDefaults.standard.object(forKey: "FavoritedChannelService.favoritedChannels") as? Data else {
            return [Int]()
        }

        guard let favoritedChannels = NSKeyedUnarchiver.unarchiveObject(with: cacheData) as? [Int] else {
            return [Int]()
        }

        return favoritedChannels
    }

    internal func clearFavoritedChannelsCache() {
        UserDefaults.standard
            .set(nil
                 , forKey: "FavoritedChannelService.favoritedChannels")
    }
}
