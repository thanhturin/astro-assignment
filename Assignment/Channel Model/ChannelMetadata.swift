//
//  ChannelMetadata.swift
//  Assignment
//
//  Created by Thanh KFit on 3/6/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import Foundation
import RxSwift

final class ChannelMetadata {
    let channelId: Int
    let channelTitle: String
    let channelDescription: String?
    let channelLanguage: String?
    let channelCategory: String?
    let channelStbNumber: String?
    let channelHD: Bool?
    let hdSimulcastChannel: Int?
    let channelStartDate: String?
    let channelEndDate: String?
    let channelExtRefs: [ChannelExtRef]
    let linearOttMappings: [LinearOttMapping]

    init(
        channelId: Int
        , channelTitle: String
        , channelDescription: String?
        , channelLanguage: String?
        , channelCategory: String?
        , channelStbNumber: String?
        , channelHD: Bool?
        , hdSimulcastChannel: Int?
        , channelStartDate: String?
        , channelEndDate: String?
        , channelExtRefs: [ChannelExtRef]
        , linearOttMappings: [LinearOttMapping]
    ) {
        self.channelId = channelId
        self.channelTitle = channelTitle
        self.channelDescription = channelDescription
        self.channelLanguage = channelLanguage
        self.channelCategory = channelCategory
        self.channelStbNumber = channelStbNumber
        self.channelHD = channelHD
        self.hdSimulcastChannel = hdSimulcastChannel
        self.channelStartDate = channelStartDate
        self.channelEndDate = channelEndDate
        self.channelExtRefs = channelExtRefs
        self.linearOttMappings = linearOttMappings
    }
}

extension ChannelMetadata: Serializable {
    final class func serialize(_ jsonRepresentation: AnyObject?) -> ChannelMetadata? {
        guard let json = jsonRepresentation as? [String: AnyObject] else {
            return nil
        }

        // Required
        guard let channelId = json["channelId"] as? Int else {
            return nil
        }

        guard let channelTitle = json["channelTitle"] as? String else {
            return nil
        }

        // Optional
        let channelDescription = json["channelDescription"] as? String
        let channelLanguage = json["channelLanguage"] as? String
        let channelCategory = json["channelCategory"] as? String
        let channelStbNumber = json["channelStbNumber"] as? String
        let channelHD = json["channelHD"] as? Bool
        let hdSimulcastChannel = json["hdSimulcastChannel"] as? Int
        let channelStartDate = json["channelStartDate"] as? String
        let channelEndDate = json["channelEndDate"] as? String

        // Array
        var channelExtRefs = [ChannelExtRef]()
        if let items = json["channelExtRefs"] as? [AnyObject] {
            for item in items {
                if let channelExtRef = ChannelExtRef.serialize(item) {
                    channelExtRefs.append(channelExtRef)
                }
            }
        }

        var linearOttMappings = [LinearOttMapping]()
        if let items = json["linearOttMappings"] as? [AnyObject] {
            for item in items {
                if let linearOttMapping = LinearOttMapping.serialize(item) {
                    linearOttMappings.append(linearOttMapping)
                }
            }
        }

        let result = ChannelMetadata(
            channelId: channelId
            , channelTitle: channelTitle
            , channelDescription: channelDescription
            , channelLanguage: channelLanguage
            , channelCategory: channelCategory
            , channelStbNumber: channelStbNumber
            , channelHD: channelHD
            , hdSimulcastChannel: hdSimulcastChannel
            , channelStartDate: channelStartDate
            , channelEndDate: channelEndDate
            , channelExtRefs: channelExtRefs
            , linearOttMappings: linearOttMappings)
        return result
    }
}
