//
//  ChannelExtRef.swift
//  Assignment
//
//  Created by Thanh KFit on 3/6/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import Foundation
import RxSwift

final class ChannelExtRef {

    let system: String
    let subSystem: String
    let value: String

    init(
        system: String
        , subSystem: String
        , value: String
    ) {
        self.system = system
        self.subSystem = subSystem
        self.value = value
    }
}

extension ChannelExtRef: Serializable {
    final class func serialize(_ jsonRepresentation: AnyObject?) -> ChannelExtRef? {
        guard let json = jsonRepresentation as? [String: AnyObject] else {
            return nil
        }

        // Required
        guard let system = json["system"] as? String else {
            return nil
        }
        guard let subSystem = json["subSystem"] as? String else {
            return nil
        }
        guard let value = json["value"] as? String else {
            return nil
        }

        let result = ChannelExtRef(
            system: system
            , subSystem: subSystem
            , value: value
        )
        return result
    }
}
