//
//  Channel.swift
//  Assignment
//
//  Created by Thanh KFit on 3/5/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import Foundation
import RxSwift

protocol ChannelType {
    var channelId: Int { get }
    var channelTitle: String { get }
    var channelStbNumber: Int { get }
}

final class Channel: NSObject, NSCoding, ChannelType {

    let channelId: Int
    let channelTitle: String
    let channelStbNumber: Int

    init(
        channelId: Int
        , channelTitle: String
        , channelStbNumber: Int
    ) {
        self.channelId = channelId
        self.channelTitle = channelTitle
        self.channelStbNumber = channelStbNumber
    }

    // MARK: NSCoding (if needed)
    @objc required convenience init?(coder aDecoder: NSCoder) {
        // Required
        guard let channelId = aDecoder.decodeObject(forKey: "Channel.channelId") as? Int else {
            return nil
        }
        guard let channelTitle = aDecoder.decodeObject(forKey: "Channel.channelTitle") as? String else {
            return nil
        }
        guard let channelStbNumber = aDecoder.decodeObject(forKey: "Channel.channelStbNumber") as? Int else {
            return nil
        }

        self.init(channelId: channelId, channelTitle: channelTitle, channelStbNumber: channelStbNumber)
    }

    @objc func encode(with aCoder: NSCoder) {
        aCoder.encode((channelId as AnyObject), forKey: "Channel.channelId")
        aCoder.encode((channelTitle as AnyObject), forKey: "Channel.channelTitle")
        aCoder.encode((channelStbNumber as AnyObject), forKey: "Channel.channelStbNumber")
    }
}

extension Channel: Serializable {
    final class func serialize(_ jsonRepresentation: AnyObject?) -> Channel? {
        guard let json = jsonRepresentation as? [String: AnyObject] else {
            return nil
        }

        // Required
        guard let channelId = json["channelId"] as? Int else {
            return nil
        }

        guard let channelTitle = json["channelTitle"] as? String else {
            return nil
        }

        guard let channelStbNumber = json["channelStbNumber"] as? Int else {
            return nil
        }

        let result = Channel(channelId: channelId, channelTitle: channelTitle, channelStbNumber: channelStbNumber)
        return result
    }
}
