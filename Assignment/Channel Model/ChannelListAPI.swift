//
//  ChannelListAPI.swift
//  Assignment
//
//  Created by Thanh KFit on 3/5/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire

struct ChannelListAPIRequestPayload {

    init(
    ) {
    }
}

extension ChannelListAPIRequestPayload: Deserializable {
    func deserialize() -> [String: AnyObject] {
        let parameters = [String: AnyObject]()
        return parameters
    }
}

protocol ChannelListAPI {
    func request(withRequestPayload requestPayload: ChannelListAPIRequestPayload) -> Observable<ChannelListAPIResponsePayload>
}

final class ChannelListAPIDefault: ChannelListAPI {

    private let apiService: APIService
    private let networkService: NetworkService

    init(apiService: APIService = apiServiceDefault
         , networkService: NetworkService = networkServiceDefault
    ) {
        self.apiService = apiService
        self.networkService = networkService
    }

    func request(withRequestPayload requestPayload: ChannelListAPIRequestPayload) -> Observable<ChannelListAPIResponsePayload> {

        let URL = apiService.baseURL.appendingPathComponent("/ams/v3/getChannelList")

        let result = Observable.create {
            (observer: AnyObserver<ChannelListAPIResponsePayload>) -> Disposable in

            let parameters = requestPayload.deserialize()
            let urlRequest = try! self.networkService
                .urlRequestConvertible(method: .get, url: URL, parameters: parameters, encoding: URLEncoding.default, headers: nil)

            let request = self.networkService.managerWithDefaultConfiguration.request(urlRequest)

            request.responseObject {
                (response: DataResponse<ChannelListAPIResponsePayload>) -> Void in
                if let value = response.result.value {
                    observer.onNext(value)
                }
                if let error = response.result.error {
                    observer.onError(error)
                }
                observer.on(.completed)
            }
            return Disposables.create(with: { request.cancel() })
        }
        return result
    }
}

struct ChannelListAPIResponsePayload {
    let responseMessage: String?
    let responseCode: String?
    let channels: [Channel]

    init(responseMessage: String?
         , responseCode: String?
         , channels: [Channel]) {
        self.responseMessage = responseMessage
        self.responseCode = responseCode
        self.channels = channels
    }
}

extension ChannelListAPIResponsePayload: Serializable {
    static func serialize(_ jsonRepresentation: AnyObject?) -> ChannelListAPIResponsePayload? {
        guard let json = jsonRepresentation as? [String: AnyObject] else {
            return nil
        }

        // Array
        var channels = [Channel]()
        if let items = json["channels"] as? [AnyObject] {
            for item in items {
                if let channel = Channel.serialize(item) {
                    channels.append(channel)
                }
            }
        }

        // Optional
        let responseMessage = json["responseMessage"] as? String
        let responseCode = json["responseCode"] as? String

        let result = ChannelListAPIResponsePayload(
            responseMessage: responseMessage
            , responseCode: responseCode
            , channels: channels
        )
        return result
    }
}
