//
//  ChannelMetadataAPI.swift
//  Assignment
//
//  Created by Thanh KFit on 3/6/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire

struct ChannelMetadataAPIRequestPayload {
    let channelIds: [String]
    let languages: [String]
    let categories: [String]
    let platforms: [String]

    init(
        channelIds: [String]
        , languages: [String]
        , categories: [String]
        , platforms: [String]
    ) {
        self.channelIds = channelIds
        self.languages = languages
        self.categories = categories
        self.platforms = platforms
    }
}

extension ChannelMetadataAPIRequestPayload: Deserializable {
    func deserialize() -> [String: AnyObject] {
        var parameters = [String: AnyObject]()

        parameters["channelId"] = channelIds as AnyObject?
        parameters["language"] = languages as AnyObject?
        parameters["category"] = categories as AnyObject?
        parameters["platform"] = platforms as AnyObject?

        return parameters
    }
}

protocol ChannelMetadataAPI {
    func request(withRequestPayload requestPayload: ChannelMetadataAPIRequestPayload) -> Observable<ChannelMetadataAPIResponsePayload>
}

final class ChannelMetadataAPIDefault: ChannelMetadataAPI {

    private let apiService: APIService
    private let networkService: NetworkService

    init(apiService: APIService = apiServiceDefault
         , networkService: NetworkService = networkServiceDefault
    ) {
        self.apiService = apiService
        self.networkService = networkService
    }

    func request(withRequestPayload requestPayload: ChannelMetadataAPIRequestPayload) -> Observable<ChannelMetadataAPIResponsePayload> {

        let URL = apiService.baseURL.appendingPathComponent("/ams/v3/getChannels")

        let result = Observable.create {
            (observer: AnyObserver<ChannelMetadataAPIResponsePayload>) -> Disposable in

            let parameters = requestPayload.deserialize()
            let urlRequest = try! self.networkService
                .urlRequestConvertible(method: .get, url: URL, parameters: parameters, encoding: URLEncoding.default, headers: nil)

            let request = self.networkService.managerWithDefaultConfiguration.request(urlRequest)

            request.responseObject {
                (response: DataResponse<ChannelMetadataAPIResponsePayload>) -> Void in
                if let value = response.result.value {
                    observer.onNext(value)
                }
                if let error = response.result.error {
                    observer.onError(error)
                }
                observer.on(.completed)
            }
            return Disposables.create(with: { request.cancel() })
        }
        return result
    }
}

struct ChannelMetadataAPIResponsePayload {

    let responseMessage: String?
    let responseCode: String?
    let channels: [ChannelMetadata]

    init(responseMessage: String?
         , responseCode: String?
         , channels: [ChannelMetadata]) {
        self.responseMessage = responseMessage
        self.responseCode = responseCode
        self.channels = channels
    }
}

extension ChannelMetadataAPIResponsePayload: Serializable {
    static func serialize(_ jsonRepresentation: AnyObject?) -> ChannelMetadataAPIResponsePayload? {
        guard let json = jsonRepresentation as? [String: AnyObject] else {
            return nil
        }

        // Array
        var channels = [ChannelMetadata]()
        if let items = json["channel"] as? [AnyObject] {
            for item in items {
                if let channel = ChannelMetadata.serialize(item) {
                    channels.append(channel)
                }
            }
        }

        // Optional
        let responseMessage = json["responseMessage"] as? String
        let responseCode = json["responseCode"] as? String

        let result = ChannelMetadataAPIResponsePayload(
            responseMessage: responseMessage
            , responseCode: responseCode
            , channels: channels
        )
        return result
    }
}
