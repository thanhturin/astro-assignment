//
//  LinearOttMapping.swift
//  Assignment
//
//  Created by Thanh KFit on 3/6/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import Foundation
import RxSwift

final class LinearOttMapping {

    let platform: String

    init(
        platform: String
    ) {
        self.platform = platform
    }
}

extension LinearOttMapping: Serializable {
    final class func serialize(_ jsonRepresentation: AnyObject?) -> LinearOttMapping? {
        guard let json = jsonRepresentation as? [String: AnyObject] else {
            return nil
        }

        // Required
        guard let platform = json["platform"] as? String else {
            return nil
        }

        let result = LinearOttMapping(
            platform: platform
        )
        return result
    }
}
